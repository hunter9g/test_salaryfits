import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import 'package:forecasting/modules/forecast/data/repositories/forecast.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/external/datasource/forecast_implementation.dart';
import 'package:forecasting/modules/forecast/services/http_helper.dart';

void main() {
  ForecastApiDataSource forecastApiDataSource =
      ForecastApiDataSource(httpHelper: HttpHelper());
  ForecastRepositoryImplementation forecastRepositoryImplementation =
      ForecastRepositoryImplementation(
          forecastApiDataSource: forecastApiDataSource);

  test(
      'expected to get converted correctly to model ForecastFiveDaysModel [forecast]',
      () async {
    Either<FailGetForecast, ForecastFiveDaysModel> forecastResponse =
        await forecastRepositoryImplementation.getForecast(
            lat: -23.200024466066132, lon: -46.78105729877727);
    late ForecastFiveDaysModel? forecastFiveDaysModel;
    forecastResponse.fold((l) => null, (r) => forecastFiveDaysModel = r);

    expect(forecastFiveDaysModel, isNotNull);
  });

  test(
      'expected to get coverted correctly to model CurrentForecastModel [weather]',
      () async {
    Either<FailGetForecast, CurrentForecastModel> weatherResponse =
        await forecastRepositoryImplementation.getWeather(
            lat: -23.200024466066132, lon: -46.78105729877727);
    late CurrentForecastModel? currentForecastModel;
    weatherResponse.fold((l) => null, (r) => currentForecastModel = r);

    expect(currentForecastModel, isNotNull);
  });
}
