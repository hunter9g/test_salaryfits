import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/external/datasource/forecast_implementation.dart';
import 'package:forecasting/modules/forecast/services/http_helper.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  ForecastApiDataSource forecastApiDataSource =
      ForecastApiDataSource(httpHelper: HttpHelper());

  test('expected to get httphelper with result [forecast]', () async {
    Either<FailGetForecast, HttpHelper> forecastResponse =
        await forecastApiDataSource.getForecast(
            lat: -23.200024466066132, lon: -46.78105729877727);
    late HttpHelper httpHelper;
    forecastResponse.fold((l) => null, (r) => httpHelper = r);

    expect(httpHelper.response, isNotNull);
  });

  test('expected to get httphelper with result [weather]', () async {
    Either<FailGetForecast, HttpHelper> forecastResponse =
        await forecastApiDataSource.getWeather(
            lat: -23.200024466066132, lon: -46.78105729877727);
    late HttpHelper httpHelper;
    forecastResponse.fold((l) => null, (r) => httpHelper = r);

    expect(httpHelper.response, isNotNull);
  });
}
