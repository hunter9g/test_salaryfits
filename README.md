# forecasting

A new Flutter project.

##### caso você não tenha o flutter instalado

https://docs.flutter.dev/get-started/install

##### após instalar e/ou já tiver o flutter

#### navegue para dentro do projeto

cd test_salaryfits

#### instale as dependencias

flutter pub get

#### após instalar as dependencias você pode rodar o projeto

flutter run

#### para rodar os testes

flutter test

#### imagens das telas

#### para abrir a segunda tela basta tocar no nome da localização ex: Mountain View, US.

![Screenshot](home_screen.png)
![Screenshot](forecast_five_days.png)
![Screenshot](forecast_five_days_light_theme_and_user_settings.png)
![Screenshot](theme_with_portuguese_translations.png)
