import 'package:forecasting/modules/forecast/data/repositories/forecast.dart';
import 'package:forecasting/modules/forecast/data/repositories/location.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_current_weather.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_forecast.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_location.dart';
import 'package:forecasting/modules/forecast/external/datasource/forecast_implementation.dart';
import 'package:forecasting/modules/forecast/external/datasource/location_implementation.dart';
import 'package:forecasting/modules/forecast/services/geolocator_helper.dart';
import 'package:forecasting/modules/forecast/services/http_helper.dart';
import 'package:forecasting/src/bloc/location/bloc.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/bloc/weather/bloc.dart';
import 'package:get_it/get_it.dart';
import 'src/bloc/forecast/bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerFactory<ForecastBloc>(() => ForecastBloc(
      getCurrentForecastUsecase: sl()));

  sl.registerLazySingleton<ForecastRepositoryImplementation>(
      () => ForecastRepositoryImplementation(forecastApiDataSource: sl()));

  sl.registerLazySingleton<WeatherBloc>(
      () => WeatherBloc(getCurrentWeatherUsecase: sl()));

  sl.registerLazySingleton<GetCurrentForecastUsecase>(
      () => GetCurrentForecastUsecase(forecastRepositoryImplementation: sl()));

  sl.registerLazySingleton<GetCurrentWeatherUsecase>(
      () => GetCurrentWeatherUsecase(forecastRepositoryImplementation: sl()));

  sl.registerLazySingleton<ForecastApiDataSource>(
      () => ForecastApiDataSource(httpHelper: sl()));

  sl.registerFactory<LocationBloc>(
      () => LocationBloc(getLocationUsecase: sl()));

  sl.registerLazySingleton<LocationRepositoryImplementation>(
      () => LocationRepositoryImplementation(locationImplDataSource: sl()));

  sl.registerLazySingleton<GetLocationUsecase>(
      () => GetLocationUsecase(locationRepositoryImplementation: sl()));

  sl.registerLazySingleton<LocationImplDataSource>(
      () => LocationImplDataSource(geolocatorHelper: sl()));

  sl.registerFactory<UserPreferencesBloc>(() => UserPreferencesBloc());

  sl.registerLazySingleton(() => HttpHelper());

  sl.registerLazySingleton(() => GeolocatorHelper());
}
