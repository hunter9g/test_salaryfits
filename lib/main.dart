import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/forecast/bloc.dart';
import 'package:forecasting/src/bloc/location/bloc.dart';
import 'package:forecasting/src/bloc/location/event.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/bloc/user_preferences/state.dart';
import 'package:forecasting/src/bloc/weather/bloc.dart';
import 'package:forecasting/src/routes/router.dart';
import 'package:forecasting/theme/dark.dart';
import 'package:forecasting/theme/light.dart';
import 'package:localization/localization.dart';
import './injection_container.dart' as ic;

void main() async {
  await ic.init();
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final AppRouter appRouter = AppRouter();
  UserPreferencesBloc userPreferencesBloc = ic.sl<UserPreferencesBloc>();
  List<LocalizationsDelegate> localizations = [
    LocalJsonLocalization.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ];
  List<Locale> listLocales = const [
    Locale('en', 'US'),
    Locale('pt', 'BR'),
  ];

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    LocalJsonLocalization.delegate.directories = ['lib/i18n'];
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserPreferencesBloc>(
            lazy: false, create: ((context) => userPreferencesBloc)),
        BlocProvider<ForecastBloc>(
            create: ((context) => ic.sl<ForecastBloc>())),
        BlocProvider<WeatherBloc>(lazy: false, create: ((context) => ic.sl<WeatherBloc>())),
        BlocProvider<LocationBloc>(
            lazy: false,
            create: ((context) => ic.sl<LocationBloc>()..add(LocationFetch()))),
      ],
      child: BlocBuilder<UserPreferencesBloc, UserPreferencesState>(
          bloc: userPreferencesBloc,
          builder: (BuildContext buildContext, state) {
            switch (state.runtimeType) {
              case UserPreferencesLoadingState:
                return const MaterialApp();
              case UserPreferencesLoadedState:
                UserPreferencesModel userPreferencesModel =
                    state.props[0] as UserPreferencesModel;

                return MaterialApp(
                  locale: userPreferencesModel.userLocale,
                  localizationsDelegates: localizations,
                  supportedLocales: listLocales,
                  themeMode: userPreferencesModel.themeMode,
                  title: 'Forecasting',
                  theme: lightTheme,
                  darkTheme: darkTheme,
                  initialRoute: '/',
                  onGenerateRoute: appRouter.onGenerateRoute,
                );
              default:
                return const MaterialApp();
            }
          }),
    );
  }

  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.dispose();
  }
}
