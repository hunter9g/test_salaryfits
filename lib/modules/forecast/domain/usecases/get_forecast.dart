import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';

import '../../data/repositories/forecast.dart';
import '../errors/errors.dart';

class GetCurrentForecastUsecase {
  final ForecastRepositoryImplementation forecastRepositoryImplementation;
  GetCurrentForecastUsecase({required this.forecastRepositoryImplementation});
  Future<Either<FailGetForecast, ForecastFiveDaysModel>> call(
      {required double lat, required double lon}) async {
    return forecastRepositoryImplementation.getForecast(lat: lat, lon: lon);
  }
}
