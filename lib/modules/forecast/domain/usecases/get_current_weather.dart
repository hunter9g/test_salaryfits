import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';

import '../../data/repositories/forecast.dart';
import '../errors/errors.dart';

class GetCurrentWeatherUsecase {
  final ForecastRepositoryImplementation forecastRepositoryImplementation;
  GetCurrentWeatherUsecase({required this.forecastRepositoryImplementation});
  Future<Either<FailGetForecast, CurrentForecastModel>> call(
      {required double lat, required double lon}) async {
    return forecastRepositoryImplementation.getWeather(lat: lat, lon: lon);
  }
}
