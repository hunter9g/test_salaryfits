import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/modules/forecast/data/repositories/location.dart';

import '../errors/errors.dart';

class GetLocationUsecase {
  final LocationRepositoryImplementation locationRepositoryImplementation;
  GetLocationUsecase({required this.locationRepositoryImplementation});
  Future<Either<FailGetLocation, LocationModel>> call() async {
    return locationRepositoryImplementation.getLocation();
  }
}
