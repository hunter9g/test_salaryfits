import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import '../errors/errors.dart';

abstract class LocationRepository {
  Future<Either<FailGetLocation, LocationModel>> getLocation();
}
