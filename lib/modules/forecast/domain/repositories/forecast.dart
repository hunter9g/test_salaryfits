import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import '../errors/errors.dart';

abstract class ForecastRepository {
  Future<Either<FailGetForecast, ForecastFiveDaysModel>> getForecast(
      {required double lat, required double lon});

  Future<Either<FailGetForecast, CurrentForecastModel>> getWeather(
      {required double lat, required double lon});
}
