import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import '../errors/errors.dart';

abstract class UserPreferencesRepository {
  Future<Either<FailGetPreferencesError, UserPreferencesModel>>
      getUserPreferences();

  Future<Either<FailGetPreferencesError, UserPreferencesModel>>
      setUserPreferences(UserPreferencesModel userPreferencesModel);
}
