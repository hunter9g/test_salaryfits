abstract class ForecastError implements Exception {}

class FailGetForecast implements ForecastError {
  String message;
  final int? statusCode;
  FailGetForecast({required this.message, this.statusCode});
}

abstract class LocationError implements Exception {}

class FailGetLocation implements LocationError {
  String message;
  FailGetLocation({required this.message});
}

abstract class FailGetPreferencesError implements Exception {}
