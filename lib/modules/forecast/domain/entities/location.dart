class LocationEntity {
  double lat;
  double lon;
  LocationEntity({
    required this.lat,
    required this.lon,
  });
}
