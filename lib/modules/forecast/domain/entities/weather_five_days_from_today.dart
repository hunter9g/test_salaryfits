import 'package:forecasting/modules/forecast/domain/entities/current_weather.dart';

class ForecastFiveDaysEntity {
  String? cod;
  double? message;
  double? cnt;
  List<DataEntity>? list;
  CityEntity? city;

  ForecastFiveDaysEntity(
      {this.cod, this.message, this.cnt, this.list, this.city});
}

class DataEntity {
  int? dt;
  MainEntity? main;
  List<WeatherEntity>? weather;
  CloudsEntity? clouds;
  WindEntity? wind;
  double? visibility;
  double? pop;
  SysEntity? sys;
  String? dtTxt;
  RainEntity? rain;

  DataEntity(
      {this.dt,
      this.main,
      this.weather,
      this.clouds,
      this.wind,
      this.visibility,
      this.pop,
      this.sys,
      this.dtTxt,
      this.rain});
}

class MainEntity {
  double? temp;
  double? feelsLike;
  double? tempMin;
  double? tempMax;
  int? pressure;
  int? seaLevel;
  int? grndLevel;
  int? humidity;
  double? tempKf;

  MainEntity(
      {this.temp,
      this.feelsLike,
      this.tempMin,
      this.tempMax,
      this.pressure,
      this.seaLevel,
      this.grndLevel,
      this.humidity,
      this.tempKf});
}

class RainEntity {
  double? d3h;

  RainEntity({this.d3h});
}

class CityEntity {
  int? id;
  String? name;
  CoordEntity? coord;
  String? country;
  int? population;
  // int? timezone;
  int? sunrise;
  int? sunset;

  CityEntity(
      {this.id,
      this.name,
      this.coord,
      this.country,
      this.population,
      // this.timezone,
      this.sunrise,
      this.sunset});
}

class Coord {
  double? lat;
  double? lon;

  Coord({this.lat, this.lon});

  Coord.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lon = json['lon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lat'] = lat;
    data['lon'] = lon;
    return data;
  }
}
