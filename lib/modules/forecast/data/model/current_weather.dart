// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';
import 'package:forecasting/modules/forecast/domain/entities/current_weather.dart';

class CurrentForecastModel extends CurrentForecastEntity {
  CoordModel? coordModel;
  List<WeatherModel>? weatherModel;
  String? baseModel;
  MainModel? mainModel;
  int? visibilityModel;
  WindModel? windModel;
  CloudsModel? cloudsModel;
  int? dtModel;
  SysModel? sysModel;
  int? timezoneModel;
  int? idModel;
  String? nameModel;
  int? codModel;
  CurrentForecastModel({
    this.coordModel,
    this.weatherModel,
    this.baseModel,
    this.mainModel,
    this.visibilityModel,
    this.windModel,
    this.cloudsModel,
    this.dtModel,
    this.sysModel,
    this.timezoneModel,
    this.idModel,
    this.nameModel,
    this.codModel,
  }) : super(
            coord: coordModel,
            weather: weatherModel,
            base: baseModel,
            main: mainModel,
            visibility: visibilityModel,
            wind: windModel,
            clouds: cloudsModel,
            dt: dtModel,
            sys: sysModel,
            timezone: timezoneModel,
            id: idModel,
            name: nameModel,
            cod: codModel);

  CurrentForecastModel.fromJson(Map<String, dynamic> json) {
    coordModel =
        json['coord'] != null ? CoordModel.fromJson(json['coord']) : null;
    if (json['weather'] != null) {
      weatherModel = <WeatherModel>[];
      json['weather'].forEach((v) {
        weatherModel!.add(WeatherModel.fromJson(v));
      });
    }
    baseModel = json['base'];
    mainModel = json['main'] != null ? MainModel.fromJson(json['main']) : null;
    visibilityModel = json['visibility'];
    windModel = json['wind'] != null ? WindModel.fromJson(json['wind']) : null;
    cloudsModel =
        json['clouds'] != null ? CloudsModel.fromJson(json['clouds']) : null;
    dtModel = json['dt'];
    sysModel = json['sys'] != null ? SysModel.fromJson(json['sys']) : null;
    timezoneModel = json['timezone'];
    idModel = json['id'];
    nameModel = json['name'];
    codModel = json['cod'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (coordModel != null) {
      data['coord'] = coordModel!.toJson();
    }
    if (weatherModel != null) {
      data['weather'] = weatherModel!.map((v) => v.toJson()).toList();
    }
    data['base'] = baseModel;
    if (mainModel != null) {
      data['main'] = mainModel!.toJson();
    }
    data['visibility'] = visibilityModel;
    if (windModel != null) {
      data['wind'] = windModel!.toJson();
    }
    if (cloudsModel != null) {
      data['clouds'] = cloudsModel!.toJson();
    }
    data['dt'] = dtModel;
    if (sysModel != null) {
      data['sys'] = sysModel!.toJson();
    }
    data['timezone'] = timezoneModel;
    data['id'] = idModel;
    data['name'] = nameModel;
    data['cod'] = codModel;
    return data;
  }

  CurrentForecastModel copyWith({
    CoordModel? coord,
    List<WeatherModel>? weather,
    String? base,
    MainModel? main,
    int? visibility,
    WindModel? wind,
    CloudsModel? clouds,
    int? dt,
    SysModel? sys,
    int? timezone,
    int? id,
    String? name,
    int? cod,
  }) {
    return CurrentForecastModel(
      coordModel: coord ?? coordModel,
      weatherModel: weather ?? weatherModel,
      baseModel: base ?? baseModel,
      mainModel: main ?? mainModel,
      visibilityModel: visibility ?? visibilityModel,
      windModel: wind ?? windModel,
      cloudsModel: clouds ?? cloudsModel,
      dtModel: dt ?? dtModel,
      sysModel: sys ?? sysModel,
      timezoneModel: timezone ?? timezoneModel,
      idModel: id ?? idModel,
      nameModel: name ?? nameModel,
      codModel: cod ?? codModel,
    );
  }

  // Map<String, dynamic> toMap() {
  //   return <String, dynamic>{
  //     'coord': coordModel?.toMap(),
  //     'weather': weatherModel!.map((x) => x.toMap()).toList(),
  //     'base': baseModel,
  //     'main': mainModel?.toMap(),
  //     'visibility': visibilityModel,
  //     'wind': windModel?.toMap(),
  //     'clouds': cloudsModel?.toMap(),
  //     'dt': dtModel,
  //     'sys': sysModel?.toMap(),
  //     'timezone': timezoneModel,
  //     'id': idModel,
  //     'name': nameModel,
  //     'cod': codModel,
  //   };
  // }

  // factory CurrentForecastModel.fromMap(Map<String, dynamic> map) {
  //   return CurrentForecastModel(
  //     coordModel: map['coord'] != null
  //         ? CoordModel.fromMap(map['coord'] as Map<String, dynamic>)
  //         : null,
  //     weatherModel: map['weather'] != null
  //         ? List<WeatherModel>.from(
  //             (map['weather'] as List<int>).map<WeatherModel?>(
  //               (x) => WeatherModel.fromMap(x as Map<String, dynamic>),
  //             ),
  //           )
  //         : null,
  //     baseModel: map['base'] != null ? map['base'] as String : null,
  //     mainModel: map['main'] != null
  //         ? MainModel.fromMap(map['main'] as Map<String, dynamic>)
  //         : null,
  //     visibility: map['visibility'] != null ? map['visibility'] as int : null,
  //     wind: map['wind'] != null
  //         ? Wind.fromMap(map['wind'] as Map<String, dynamic>)
  //         : null,
  //     clouds: map['clouds'] != null
  //         ? Clouds.fromMap(map['clouds'] as Map<String, dynamic>)
  //         : null,
  //     dt: map['dt'] != null ? map['dt'] as int : null,
  //     sys: map['sys'] != null
  //         ? Sys.fromMap(map['sys'] as Map<String, dynamic>)
  //         : null,
  //     timezone: map['timezone'] != null ? map['timezone'] as int : null,
  //     id: map['id'] != null ? map['id'] as int : null,
  //     name: map['name'] != null ? map['name'] as String : null,
  //     cod: map['cod'] != null ? map['cod'] as int : null,
  //   );
  // }
}

class CoordModel extends CoordEntity {
  double? lonModel;
  double? latModel;
  CoordModel({
    this.lonModel,
    this.latModel,
  }) : super(lon: lonModel, lat: latModel);

  CoordModel.fromJson(Map<String, dynamic> json) {
    lonModel = json['lon'];
    latModel = json['lat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lon'] = lonModel;
    data['lat'] = latModel;
    return data;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'lon': lonModel,
      'lat': latModel,
    };
  }

  factory CoordModel.fromMap(Map<String, dynamic> map) {
    return CoordModel(
      lonModel: map['lon'] != null ? map['lon'] as double : null,
      latModel: map['lat'] != null ? map['lat'] as double : null,
    );
  }
}

class WeatherModel extends WeatherEntity {
  int? idModel;
  String? mainModel;
  String? descriptionModel;
  String? iconModel;

  WeatherModel(
      {this.idModel, this.mainModel, this.descriptionModel, this.iconModel})
      : super(
            id: idModel,
            main: mainModel,
            description: descriptionModel,
            icon: iconModel);

  WeatherModel.fromJson(Map<String, dynamic> json) {
    idModel = json['id'];
    mainModel = json['main'];
    descriptionModel = json['description'];
    iconModel = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = idModel;
    data['main'] = mainModel;
    data['description'] = descriptionModel;
    data['icon'] = iconModel;
    return data;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': idModel,
      'main': mainModel,
      'description': descriptionModel,
      'icon': iconModel,
    };
  }

  factory WeatherModel.fromMap(Map<String, dynamic> map) {
    return WeatherModel(
      idModel: map['id'] != null ? map['id'] as int : null,
      mainModel: map['main'] != null ? map['main'] as String : null,
      descriptionModel:
          map['description'] != null ? map['description'] as String : null,
      iconModel: map['icon'] != null ? map['icon'] as String : null,
    );
  }

  Widget returnImageBasedOnWeather() {
    final Map<String, String> assets = {
      'Clouds': 'assets/img/cloud.png',
      'Clear': 'assets/img/sun.png'
    };

    return Image.asset(
      assets[mainModel] ?? 'assets/img/sun.png',
      width: 120,
    );
  }
}

class MainModel extends MainEntity {
  double? tempModel;
  double? feelsLikeModel;
  double? tempMinModel;
  double? tempMaxModel;
  int? pressureModel;
  int? humidityModel;
  int? seaLevelModel;
  // int? grndLevelModel;

  MainModel({
    this.tempModel,
    this.feelsLikeModel,
    this.tempMinModel,
    this.tempMaxModel,
    this.pressureModel,
    this.humidityModel,
    this.seaLevelModel,
    // this.grndLevelModel,
  }) : super(
          temp: tempModel,
          feelsLike: feelsLikeModel,
          tempMin: tempMinModel,
          tempMax: tempMaxModel,
          pressure: pressureModel,
          humidity: humidityModel,
          seaLevel: seaLevelModel,
          // grndLevel: grndLevelModel
        );

  MainModel.fromJson(Map<String, dynamic> json) {
    tempModel = json['temp'].toDouble();
    feelsLikeModel = json['feels_like'].toDouble();
    tempMinModel = json['temp_min'].toDouble();
    tempMaxModel = json['temp_max'].toDouble();
    pressureModel = json['pressure'];
    humidityModel = json['humidity'];
    seaLevelModel = json['sea_level'];
    // grndLevelModel = json['grnd_level'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['temp'] = tempModel;
    data['feels_like'] = feelsLikeModel;
    data['temp_min'] = tempMinModel;
    data['temp_max'] = tempMaxModel;
    data['pressure'] = pressureModel;
    data['humidity'] = humidityModel;
    data['sea_level'] = seaLevelModel;
    // data['grnd_level'] = grndLevelModel;
    return data;
  }

  String tempToAny({required Locale locale}) {
    if (locale.toLanguageTag() == 'en-US') {
      double fahrenheit = (tempModel! - 273.15) * 9 / 5 + 32;
      return '${fahrenheit.toInt()} ºF';
    }

    if (locale.toLanguageTag() == 'pt-BR') {
      return '${(tempModel! - 273.15).toInt()} ºC';
    }
    return '${(tempModel! - 273.15).toInt()} ºC';
  }
}

class WindModel extends WindEntity {
  double? speedModel;
  int? degModel;
  // double? gustModel;
  WindModel({
    this.speedModel,
    this.degModel,
    // this.gustModel,
  }) : super(
          speed: speedModel, deg: degModel,
          // gust: gustModel
        );

  WindModel.fromJson(Map<String, dynamic> json) {
    speedModel = json['speed'].toDouble();
    degModel = json['deg'];
    // gustModel = json['gust'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['speed'] = speedModel;
    data['deg'] = degModel;
    // data['gust'] = gustModel;
    return data;
  }
}

class CloudsModel extends CloudsEntity {
  int? allModel;

  CloudsModel({this.allModel}) : super(all: allModel);

  CloudsModel.fromJson(Map<String, dynamic> json) {
    allModel = json['all'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['all'] = allModel;
    return data;
  }
}

class SysModel extends SysEntity {
  int? typeModel;
  int? idModel;
  String? countryModel;
  int? sunriseModel;
  int? sunsetModel;

  SysModel(
      {this.typeModel,
      this.idModel,
      this.countryModel,
      this.sunriseModel,
      this.sunsetModel})
      : super(
            type: typeModel,
            id: idModel,
            country: countryModel,
            sunrise: sunriseModel,
            sunset: sunsetModel);

  SysModel.fromJson(Map<String, dynamic> json) {
    typeModel = json['type'];
    idModel = json['id'];
    countryModel = json['country'];
    sunriseModel = json['sunrise'];
    sunsetModel = json['sunset'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = typeModel;
    data['id'] = idModel;
    data['country'] = countryModel;
    data['sunrise'] = sunriseModel;
    data['sunset'] = sunsetModel;
    return data;
  }
}
