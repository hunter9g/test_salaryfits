import 'package:flutter/material.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/domain/entities/weather_five_days_from_today.dart';
import 'package:intl/intl.dart';

class ForecastFiveDaysModel extends ForecastFiveDaysEntity {
  String? codModel;
  double? messageModel;
  double? cntModel;
  List<DataModel>? listModel;
  CityModel? cityModel;

  ForecastFiveDaysModel(
      {this.codModel,
      this.messageModel,
      this.cntModel,
      this.listModel,
      this.cityModel})
      : super(
            cod: codModel,
            message: messageModel,
            cnt: cntModel,
            list: listModel,
            city: cityModel);

  ForecastFiveDaysModel.fromJson(Map<String, dynamic> json) {
    codModel = json['cod'];
    messageModel = json['message'].toDouble();
    cntModel = json['cnt'].toDouble();
    if (json['list'] != null) {
      listModel = <DataModel>[];
      json['list'].forEach((v) {
        listModel!.add(DataModel.fromJson(v));
      });
    }
    cityModel = json['city'] != null ? CityModel.fromJson(json['city']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cod'] = codModel;
    data['message'] = messageModel;
    data['cnt'] = cntModel;
    if (listModel != null) {
      data['list'] = listModel!.map((v) => v.toJson()).toList();
    }
    if (cityModel != null) {
      data['city'] = cityModel!.toJson();
    }
    return data;
  }

  ForecastFiveDaysModel copyWith({
    String? cod,
    double? message,
    double? cnt,
    List<DataModel>? list,
    CityModel? city,
  }) {
    return ForecastFiveDaysModel(
      codModel: cod ?? codModel,
      messageModel: message ?? messageModel,
      cntModel: cnt ?? cntModel,
      listModel: list ?? listModel,
      cityModel: city ?? cityModel,
    );
  }

  void sortListByDate() {
    listModel!.sort((a, b) {
      return a.dt?.compareTo(b.dt ?? 0) ?? 0;
    });
  }
}

class DataModel extends DataEntity {
  int? dtModel;
  MainModel? mainModel;
  List<WeatherModel>? weatherModel;
  CloudsModel? cloudsModel;
  WindModel? windModel;
  double? visibilityModel;
  // double? popModel;
  // SysModel? sysModel;
  String? dtTxtModel;
  RainModel? rainModel;

  DataModel(
      {this.dtModel,
      this.mainModel,
      this.weatherModel,
      this.cloudsModel,
      this.windModel,
      this.visibilityModel,
      // this.popModel,
      // this.sysModel,
      this.dtTxtModel,
      this.rainModel})
      : super(
            dt: dtModel,
            main: mainModel,
            weather: weatherModel,
            clouds: cloudsModel,
            wind: windModel,
            visibility: visibilityModel,
            // pop: popModel,
            // sys: sysModel,
            dtTxt: dtTxtModel);

  DataModel.fromJson(Map<String, dynamic> json) {
    dtModel = json['dt'];
    mainModel = json['main'] != null ? MainModel.fromJson(json['main']) : null;
    if (json['weather'] != null) {
      weatherModel = <WeatherModel>[];
      json['weather'].forEach((v) {
        weatherModel!.add(WeatherModel.fromJson(v));
      });
    }
    cloudsModel =
        json['clouds'] != null ? CloudsModel.fromJson(json['clouds']) : null;
    windModel = json['wind'] != null ? WindModel.fromJson(json['wind']) : null;
    visibilityModel = json['visibility'].toDouble();
    // popModel = json['pop'];
    // sysModel = json['sys'] != null ? SysModel.fromJson(json['sys']) : null;
    dtTxtModel = json['dt_txt'];
    rainModel = json['rain'] != null ? RainModel.fromJson(json['rain']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['dt'] = dtModel;
    if (mainModel != null) {
      data['main'] = mainModel!.toJson();
    }
    if (weatherModel != null) {
      data['weather'] = weatherModel!.map((v) => v.toJson()).toList();
    }
    if (cloudsModel != null) {
      data['clouds'] = cloudsModel!.toJson();
    }
    if (cloudsModel != null) {
      data['wind'] = windModel!.toJson();
    }
    data['visibility'] = visibilityModel;
    // data['pop'] = popModel;
    // if (sysModel != null) {
    //   data['sys'] = sysModel!.toJson();
    // }
    data['dt_txt'] = dtTxtModel;
    if (rainModel != null) {
      data['rain'] = rainModel!.toJson();
    }
    return data;
  }

  String returnDateTimeInStringFromTimeStamp() {
    final f = DateFormat('dd/MM/yyyy hh:mm a');
    return f.format(DateTime.fromMillisecondsSinceEpoch(dtModel! * 1000));
  }

  String returnHourInStringFromTimeStamp() {
    final f = DateFormat('hh:mm a');
    return f.format(DateTime.fromMillisecondsSinceEpoch(dtModel! * 1000));
  }

  String returnDateInStringFromTimeStamp() {
    final f = DateFormat('dd/MM/yyyy');
    return f.format(DateTime.fromMillisecondsSinceEpoch(dtModel! * 1000));
  }
}

class MainModel extends MainEntity {
  double? tempModel;
  double? feelsLikeModel;
  double? tempMinModel;
  double? tempMaxModel;
  int? pressureModel;
  int? seaLevelModel;
  int? grndLevelModel;
  int? humidityModel;
  double? tempKfModel;

  MainModel(
      {this.tempModel,
      this.feelsLikeModel,
      this.tempMinModel,
      this.tempMaxModel,
      this.pressureModel,
      this.seaLevelModel,
      this.grndLevelModel,
      this.humidityModel,
      this.tempKfModel})
      : super(
            temp: tempModel,
            feelsLike: feelsLikeModel,
            tempMin: tempMinModel,
            tempMax: tempMaxModel,
            pressure: pressureModel,
            seaLevel: seaLevelModel,
            grndLevel: grndLevelModel,
            humidity: humidityModel,
            tempKf: tempKfModel);

  MainModel.fromJson(Map<String, dynamic> json) {
    tempModel = json['temp'].toDouble();
    feelsLikeModel = json['feels_like'].toDouble();
    tempMinModel = json['temp_min'].toDouble();
    tempMaxModel = json['temp_max'].toDouble();
    pressureModel = json['pressure'];
    seaLevelModel = json['sea_level'];
    grndLevelModel = json['grnd_level'];
    humidityModel = json['humidity'];
    tempKfModel = json['temp_kf'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['temp'] = tempModel;
    data['feels_like'] = feelsLikeModel;
    data['temp_min'] = tempMinModel;
    data['temp_max'] = tempMaxModel;
    data['pressure'] = pressureModel;
    data['sea_level'] = seaLevelModel;
    data['grnd_level'] = grndLevelModel;
    data['humidity'] = humidityModel;
    data['temp_kf'] = tempKfModel;
    return data;
  }

  String tempToAny({required Locale locale}) {
    if (locale.toLanguageTag() == 'en-US') {
      double fahrenheit = (tempModel! - 273.15) * 9 / 5 + 32;
      return '${fahrenheit.toInt()} ºF';
    }

    if (locale.toLanguageTag() == 'pt-BR') {
      return '${(tempModel! - 273.15).toInt()} ºC';
    }
    return '${(tempModel! - 273.15).toInt()} ºC';
  }
}

class RainModel extends RainEntity {
  double? d3hModel;

  RainModel({this.d3hModel}) : super(d3h: d3hModel);

  RainModel.fromJson(Map<String, dynamic> json) {
    d3hModel = json['3h'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['3h'] = d3hModel;
    return data;
  }
}

class CityModel extends CityEntity {
  int? idModel;
  String? nameModel;
  CoordModel? coordModel;
  String? countryModel;
  int? populationModel;
  // int? timezoneModel;
  int? sunriseModel;
  int? sunsetModel;
  CityModel({
    this.idModel,
    this.nameModel,
    this.coordModel,
    this.countryModel,
    this.populationModel,
    // this.timezoneModel,
    this.sunriseModel,
    this.sunsetModel,
  }) : super(
            id: idModel,
            name: nameModel,
            coord: coordModel,
            country: countryModel,
            population: populationModel,
            // timezone: timezoneModel,
            sunrise: sunriseModel,
            sunset: sunsetModel);

  CityModel.fromJson(Map<String, dynamic> json) {
    idModel = json['id'];
    nameModel = json['name'];
    coordModel =
        json['coord'] != null ? CoordModel.fromJson(json['coord']) : null;
    countryModel = json['country'];
    populationModel = json['population'];
    // timezoneModel = json['timezone'];
    sunriseModel = json['sunrise'];
    sunsetModel = json['sunset'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = idModel;
    data['name'] = nameModel;
    if (coordModel != null) {
      data['coord'] = coordModel!.toJson();
    }
    data['country'] = countryModel;
    data['population'] = populationModel;
    // data['timezone'] = timezoneModel;
    data['sunrise'] = sunriseModel;
    data['sunset'] = sunsetModel;
    return data;
  }
}
