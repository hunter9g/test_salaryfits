import 'package:forecasting/modules/forecast/domain/entities/location.dart';

class LocationModel extends LocationEntity {
  double latModel;
  double lonModel;
  LocationModel({
    required this.latModel,
    required this.lonModel,
  }) : super(lat: latModel, lon: lonModel);
}
