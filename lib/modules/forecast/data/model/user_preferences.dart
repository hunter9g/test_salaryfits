// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class UserPreferencesModel {
  final Locale userLocale;
  final ThemeMode themeMode;
  UserPreferencesModel({
    this.userLocale = const Locale('en', 'US'),
    this.themeMode = ThemeMode.light,
  });

  const UserPreferencesModel.empty(
      {this.userLocale = const Locale('en', 'US'),
      this.themeMode = ThemeMode.light});

  UserPreferencesModel copyWith({
    Locale? userLocale,
    ThemeMode? themeMode,
  }) {
    return UserPreferencesModel(
      userLocale: userLocale ?? this.userLocale,
      themeMode: themeMode ?? this.themeMode,
    );
  }

  bool isDarkTheme() {
    return themeMode == ThemeMode.dark;
  }
}
