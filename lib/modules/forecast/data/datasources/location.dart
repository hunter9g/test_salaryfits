import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:geolocator/geolocator.dart';

abstract class LocationDataSource {
  Future<Either<FailGetLocation, Position>> getLocation();
}
