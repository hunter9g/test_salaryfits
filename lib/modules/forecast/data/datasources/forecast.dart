import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';

import '../../services/http_helper.dart';

abstract class ForecastDatasource {
  Future<Either<FailGetForecast, HttpHelper>> getForecast(
      {required double lat, required double lon});

  Future<Either<FailGetForecast, HttpHelper>> getWeather(
      {required double lat, required double lon});
}
