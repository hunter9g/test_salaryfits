import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/modules/forecast/domain/repositories/location.dart';
import 'package:forecasting/modules/forecast/external/datasource/location_implementation.dart';
import 'package:geolocator/geolocator.dart';

import '../../domain/errors/errors.dart';

class LocationRepositoryImplementation extends LocationRepository {
  final LocationImplDataSource locationImplDataSource;

  LocationRepositoryImplementation({required this.locationImplDataSource});

  @override
  Future<Either<FailGetLocation, LocationModel>> getLocation() async {
    Either<FailGetLocation, Position> response =
        await locationImplDataSource.getLocation();

    return response.fold(
        (l) => Left(l),
        (r) =>
            Right(LocationModel(latModel: r.latitude, lonModel: r.longitude)));
  }
}
