import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/domain/repositories/user_preferences.dart';

class UserPreferencesRepositoryImpl implements UserPreferencesRepository {
  @override
  Future<Either<FailGetPreferencesError, UserPreferencesModel>>
      getUserPreferences() {
    throw UnimplementedError();
  }

  @override
  Future<Either<FailGetPreferencesError, UserPreferencesModel>>
      setUserPreferences(UserPreferencesModel userPreferencesModel) {
    throw UnimplementedError();
  }
}
