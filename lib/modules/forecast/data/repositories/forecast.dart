import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import 'package:forecasting/modules/forecast/services/http_helper.dart';

import '../../domain/errors/errors.dart';
import '../../domain/repositories/forecast.dart';
import '../../external/datasource/forecast_implementation.dart';

class ForecastRepositoryImplementation extends ForecastRepository {
  final ForecastApiDataSource forecastApiDataSource;

  ForecastRepositoryImplementation({required this.forecastApiDataSource});

  @override
  Future<Either<FailGetForecast, ForecastFiveDaysModel>> getForecast(
      {required double lat, required double lon}) async {
    Either<FailGetForecast, HttpHelper> response =
        await forecastApiDataSource.getForecast(lat: lat, lon: lon);

    return response.fold((l) => Left(l),
        (r) => Right(ForecastFiveDaysModel.fromJson(r.getData())));
  }

  @override
  Future<Either<FailGetForecast, CurrentForecastModel>> getWeather(
      {required double lat, required double lon}) async {
    Either<FailGetForecast, HttpHelper> response =
        await forecastApiDataSource.getWeather(lat: lat, lon: lon);

    return response.fold((l) => Left(l),
        (r) => Right(CurrentForecastModel.fromJson(r.getData())));
  }
}
