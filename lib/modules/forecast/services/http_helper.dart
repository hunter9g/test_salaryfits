import 'package:dio/dio.dart';

class ExceptionsInfos {
  final String message;
  final int? statusCode;

  ExceptionsInfos({required this.message, this.statusCode});
}

class HttpHelper {
  final Map<String, dynamic>? headers;
  late Response? response;
  late Object? data;
  Exception? exception;
  DioException? dioError;
  late ExceptionsInfos exceptionsInfos;

  HttpHelper({this.headers});

  dynamic getData() {
    return response!.data;
  }

  int getStatusCode() {
    return response!.statusCode ?? 500;
  }

  Response getResponse() {
    return response!;
  }

  Future<HttpHelper> get(
      {required String url,
      Object? data,
      Map<String, dynamic>? headers}) async {
    await dio(method: 'get', url: url, data: data, headers: headers);
    return this;
  }

  Future<HttpHelper> put(
      {required String url,
      Object? data,
      Map<String, dynamic>? headers}) async {
    await dio(method: 'put', url: url, data: data, headers: headers);
    return this;
  }

  Future<HttpHelper> post(
      {required String url,
      Object? data,
      Map<String, dynamic>? headers}) async {
    await dio(method: 'post', url: url, data: data, headers: headers);
    return this;
  }

  Future<HttpHelper> delete(
      {required String url,
      Object? data,
      Map<String, dynamic>? headers}) async {
    await dio(method: 'delete', url: url, data: data, headers: headers);
    return this;
  }

  Future<Response> dio(
      {String? method = 'get',
      required String url,
      Object? data,
      Map<String, dynamic>? headers}) async {
    final Dio dio = Dio();

    dio.options.headers = this.headers;

    if (headers != null) {
      dio.options.headers.addAll(headers);
    }

    Map<String, Function> methods = {
      'get': () => dio.get(url, data: data),
      'put': () => dio.put(url, data: data),
      'post': () => dio.post(url, data: data),
      'delete': () => dio.delete(url, data: data),
    };

    try {
      response = await methods[method]?.call();
    } on DioException catch (e) {
      dioError = e;
    } on Exception catch (e) {
      exception = e;
    }

    return response!;
  }

  bool hasExcepion() {
    return dioError != null || exception != null;
  }

  ExceptionsInfos getExceptionMessage() {
    return ExceptionsInfos(
        message:
            dioError?.response!.data['message']?.toString() ?? 'Not mapped',
        statusCode: dioError?.response?.statusCode);
  }
}
