import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import '../../data/datasources/forecast.dart';
import '../../services/http_helper.dart';

class ForecastApiDataSource implements ForecastDatasource {
  final String baseUrl = 'https://api.openweathermap.org/data/2.5/';
  final HttpHelper httpHelper;
  ForecastApiDataSource({required this.httpHelper});

  @override
  Future<Either<FailGetForecast, HttpHelper>> getForecast(
      {required double lat, required double lon}) async {
    await httpHelper.get(
        url:
            '${baseUrl}forecast?lat=$lat&lon=$lon&appid=db5a4569c1f15b66738f75c0a5ec156d');

    if (httpHelper.hasExcepion()) {
      ExceptionsInfos exceptionsInfos = httpHelper.getExceptionMessage();

      return Left(FailGetForecast(
          message: exceptionsInfos.message,
          statusCode: exceptionsInfos.statusCode));
    }

    return Right(httpHelper);
  }

  @override
  Future<Either<FailGetForecast, HttpHelper>> getWeather(
      {required double lat, required double lon}) async {
    await httpHelper.get(
        url:
            '${baseUrl}weather?lat=$lat&lon=$lon&appid=db5a4569c1f15b66738f75c0a5ec156d');

    if (httpHelper.hasExcepion()) {
      ExceptionsInfos exceptionsInfos = httpHelper.getExceptionMessage();

      return Left(FailGetForecast(
          message: exceptionsInfos.message,
          statusCode: exceptionsInfos.statusCode));
    }

    return Right(httpHelper);
  }
}
