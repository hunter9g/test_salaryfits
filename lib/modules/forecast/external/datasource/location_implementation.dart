import 'package:dartz/dartz.dart';
import 'package:forecasting/modules/forecast/data/datasources/location.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/services/geolocator_helper.dart';
import 'package:geolocator/geolocator.dart';

class LocationImplDataSource implements LocationDataSource {
  final GeolocatorHelper geolocatorHelper;
  LocationImplDataSource({required this.geolocatorHelper});

  @override
  Future<Either<FailGetLocation, Position>> getLocation() async {
    try {
      Position position = await geolocatorHelper.determinePosition();

      return Right(position);
    } catch (e) {
      return Left(FailGetLocation(
        message: 'Something went wrong',
      ));
    }
  }
}
