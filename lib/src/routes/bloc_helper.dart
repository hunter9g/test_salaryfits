import 'package:flutter_bloc/flutter_bloc.dart';

class BlocManager {
  static final BlocManager _instance = BlocManager._internal();

  factory BlocManager() {
    return _instance;
  }

  BlocManager._internal();

  final Map<Type, BlocBase> _blocs = {};

  T getBloc<T extends BlocBase>() {
    return _blocs[T] as T;
  }

  void registerBloc(BlocBase bloc) {
    _blocs[bloc.runtimeType] = bloc;
  }

  void dispose() {
    for (final bloc in _blocs.values) {
      bloc.close();
    }
    _blocs.clear();
  }
}