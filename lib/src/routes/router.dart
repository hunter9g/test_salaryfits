import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/src/bloc/forecast/bloc.dart';
import 'package:forecasting/src/bloc/location/bloc.dart';
import 'package:forecasting/src/bloc/location/event.dart';
import 'package:forecasting/src/pages/home.dart';
import 'package:forecasting/src/pages/forecast_details.dart';

import '../../injection_container.dart' as ic;

class AppRouter {
  Route onGenerateRoute(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(
        builder: (context) {
          return const HomePage();
        },
      );
    }

    if (settings.name == '/weather_details') {
      LocationModel locationModel = settings.arguments as LocationModel;
      return MaterialPageRoute(
        builder: (context) {
          return WeatherDetailsPage(locationModel: locationModel,);
        },
      );
    }

    return MaterialPageRoute(
      builder: (context) {
        return MultiBlocProvider(
          providers: [
            BlocProvider<ForecastBloc>(
                create: ((context) => ic.sl<ForecastBloc>())),
            BlocProvider<LocationBloc>(
                lazy: false,
                create: ((context) =>
                    ic.sl<LocationBloc>()..add(LocationFetch())))
          ],
          child: const HomePage(),
        );
      },
    );
  }
}
