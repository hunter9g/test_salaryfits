import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/helpers/hex_color.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/location/bloc.dart';
import 'package:forecasting/src/bloc/location/event.dart';
import 'package:forecasting/src/bloc/location/state.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/bloc/user_preferences/state.dart';
import 'package:forecasting/src/bloc/weather/bloc.dart';
import 'package:forecasting/src/bloc/weather/event.dart';
import 'package:forecasting/src/bloc/weather/state.dart';
import 'package:forecasting/src/components/loading.dart';
import 'package:forecasting/src/components/weather_status.dart';
import 'package:forecasting/src/services/utils.dart';
import 'package:localization/localization.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final WeatherBloc weatherBloc = BlocProvider.of<WeatherBloc>(context);
    final LocationBloc locationBloc = BlocProvider.of<LocationBloc>(context);
    final UserPreferencesBloc userPreferencesBloc =
        BlocProvider.of<UserPreferencesBloc>(context);

    return BlocBuilder<UserPreferencesBloc, UserPreferencesState>(
        bloc: userPreferencesBloc,
        builder: (BuildContext buildContext, state) {
          UserPreferencesModel userPreferencesModel =
              state.props[0] as UserPreferencesModel;

          return BlocBuilder<LocationBloc, LocationState>(
            bloc: locationBloc,
            builder: (context, state) {
              switch (state.runtimeType) {
                case LocationLoadingState:
                  return Scaffold(
                    body: LoadingMessage(state.props[0].toString()),
                  );
                case LocationErrorState:
                  return const Scaffold(
                    body: Center(
                      child: Text('Something went wrong with locations'),
                    ),
                  );
                default:
                  LocationModel locationModel = state.props[0] as LocationModel;

                  return Scaffold(
                    appBar: AppBar(
                      title: Text(
                        'welcome'.i18n(),
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      actions: [
                        IconButton(
                            onPressed: () => Utils(buildContext: context)
                                .showModalBottomSheetOptionsSharedPreferences(
                                    userPreferencesModel: userPreferencesModel,
                                    userPreferencesBloc: userPreferencesBloc),
                            icon: const Icon(Icons.settings)),
                        IconButton(
                            onPressed: () {
                              locationBloc.add(LocationFetch());

                              weatherBloc.add(WeatherForecastFetch(
                                  lat: locationModel.latModel,
                                  lon: locationModel.lonModel));
                            },
                            icon: const Icon(Icons.refresh)),
                      ],
                    ),
                    body: BlocBuilder<WeatherBloc, WeatherState>(
                        bloc: weatherBloc
                          ..add(WeatherForecastFetch(
                              lat: locationModel.latModel,
                              lon: locationModel.lonModel)),
                        builder: ((context, state) {
                          switch (state.runtimeType) {
                            case WeatherLoadingState:
                              return LoadingMessage(state.props[0].toString());
                            case WeatherErrorState:
                              return Text(state.props.toString());
                            case WeatherLoadedState:
                              CurrentForecastModel currentForecastModel =
                                  state.props[0] as CurrentForecastModel;
                              return SizedBox(
                                height: MediaQuery.of(context).size.height,
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.topCenter,
                                      child: Icon(
                                        Icons.cloud,
                                        size: 360,
                                        color: HexColor.createMaterialColor(
                                            Theme.of(context)
                                                .primaryColor)[200],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 24.0),
                                      child: Align(
                                        alignment: Alignment.topCenter,
                                        child: Column(
                                          children: [
                                            Text(
                                              currentForecastModel.mainModel!
                                                  .tempToAny(
                                                      locale:
                                                          userPreferencesModel
                                                              .userLocale),
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white,
                                                  fontSize: 56.0),
                                            ),
                                            Text(
                                              currentForecastModel
                                                  .weatherModel![0].mainModel!,
                                              style: TextStyle(
                                                  color: Colors.grey[300]),
                                            ),
                                            const SizedBox(
                                              height: 5.0,
                                            ),
                                            Text(
                                              'Lat: ${currentForecastModel.coordModel!.latModel!.toInt()}º Lon: ${currentForecastModel.coordModel!.lonModel!.toInt()}º',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child:
                                          Image.asset('assets/img/house.png'),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            begin: Alignment.topRight,
                                            end: Alignment.bottomLeft,
                                            colors: [
                                              Theme.of(context).primaryColor,
                                              HexColor.createMaterialColor(
                                                  Theme.of(context)
                                                      .primaryColor)[200]!,
                                            ],
                                          ),
                                          borderRadius: const BorderRadius.only(
                                              topLeft: Radius.circular(24),
                                              topRight: Radius.circular(24)),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey[300]!,
                                            ),
                                          ],
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height:
                                            MediaQuery.of(context).size.height /
                                                3.5,
                                        child: Column(
                                          children: [
                                            InkWell(
                                              onTap: () => Navigator.pushNamed(
                                                  context, '/weather_details',
                                                  arguments: locationModel),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 24.0),
                                                child: Text(
                                                  '${currentForecastModel.nameModel!}, ${currentForecastModel.sysModel!.countryModel}.',
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white,
                                                      fontSize: 24.0),
                                                ),
                                              ),
                                            ),
                                            const Divider(
                                              color: Colors.white,
                                            ),
                                            MediaQuery.of(context).size.height <
                                                    MediaQuery.of(context)
                                                        .size
                                                        .width
                                                ? const SizedBox.shrink()
                                                : Flexible(
                                                    flex: 1,
                                                    child: WeatherStatus(
                                                        currentForecastModel:
                                                            currentForecastModel)),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              );
                            default:
                              return const Text('Error not mapped');
                          }
                        })),
                  );
              }
            },
          );
        });
  }
}
