import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/forecast/bloc.dart';
import 'package:forecasting/src/bloc/forecast/event.dart';
import 'package:forecasting/src/bloc/forecast/state.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/bloc/user_preferences/state.dart';
import 'package:forecasting/src/components/card.dart';
import 'package:forecasting/src/components/loading.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:localization/localization.dart';

class WeatherDetailsPage extends StatefulWidget {
  final LocationModel locationModel;
  const WeatherDetailsPage({super.key, required this.locationModel});

  @override
  State<WeatherDetailsPage> createState() => _WeatherDetailsPageState();
}

class _WeatherDetailsPageState extends State<WeatherDetailsPage> {
  late String date;
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final UserPreferencesBloc userPreferencesBloc =
        BlocProvider.of<UserPreferencesBloc>(context);
    final ForecastBloc forecastBloc = BlocProvider.of<ForecastBloc>(context);

    return BlocProvider.value(
      value: forecastBloc
        ..add(ForecastFetchList(
            lat: widget.locationModel.latModel,
            lon: widget.locationModel.lonModel)),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'next-five-days-weather'.i18n(),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: BlocBuilder<UserPreferencesBloc, UserPreferencesState>(
          bloc: userPreferencesBloc,
          builder: (BuildContext context, state) {
            UserPreferencesModel userPreferencesModel =
                state.props[0] as UserPreferencesModel;

            return BlocBuilder<ForecastBloc, ForecastState>(
              bloc: BlocProvider.of<ForecastBloc>(context),
              builder: (context, state) {
                switch (state.runtimeType) {
                  case ForecastLoadingState:
                    return LoadingMessage(state.props[0].toString());
                  case ForecastLoadedState:
                    ForecastFiveDaysModel forecastFiveDaysModel =
                        state.props[0] as ForecastFiveDaysModel;

                    return GroupedListView<DataModel, String>(
                      useStickyGroupSeparators: true,
                      // padding: const EdgeInsets.symmetric(vertical: 6.0),
                      elements: forecastFiveDaysModel.listModel!,
                      groupBy: ((DataModel element) =>
                          element.returnDateInStringFromTimeStamp()),
                      itemBuilder:
                          (BuildContext buildContext, DataModel dataModel) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: WeatherCard(
                            dataModel: dataModel,
                            userPreferencesModel: userPreferencesModel,
                          ),
                        );
                      },
                      groupSeparatorBuilder: (String value) => Container(
                        color: Theme.of(context).primaryColor,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text(
                            value,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 24.0,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    );
                  case ForecastErrorState:
                    return const Center(
                      child: Text(
                        'Something went wrong',
                        style: TextStyle(color: Colors.white),
                      ),
                    );
                  default:
                    return const Center(
                      child: Text(
                        'Something went wrong',
                        style: TextStyle(color: Colors.white),
                      ),
                    );
                }
              },
            );
          },
        ),
      ),
    );
  }
}
