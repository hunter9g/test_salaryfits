import 'package:equatable/equatable.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';

abstract class ForecastState extends Equatable {
  const ForecastState();
}

class ForecastLoadingState extends ForecastState {
  final String message;

  const ForecastLoadingState({required this.message});
  @override
  List<Object?> get props => [message];
}

class ForecastErrorState extends ForecastState {
  final String message;

  const ForecastErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class CurrentWeatherLoadedState extends ForecastState {
  final CurrentForecastModel currentForecastModel;

  const CurrentWeatherLoadedState({required this.currentForecastModel});

  @override
  List<Object?> get props => [currentForecastModel];
}

class ForecastLoadedState extends ForecastState {
  final ForecastFiveDaysModel forecastFiveDaysModel;

  const ForecastLoadedState({required this.forecastFiveDaysModel});

  @override
  List<Object?> get props => [forecastFiveDaysModel];
}
