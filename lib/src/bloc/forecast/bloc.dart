import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_forecast.dart';
import 'event.dart';
import 'state.dart';

class ForecastBloc extends Bloc<ForecastEvent, ForecastState> {
  final GetCurrentForecastUsecase getCurrentForecastUsecase;

  ForecastBloc(
      {required this.getCurrentForecastUsecase})
      : super(const ForecastLoadingState(message: 'Searching for weather..')) {
    on<SetLoadingForecast>((event, emit) {
      emit(ForecastLoadingState(message: event.message));
    });
    on<ForecastFetchList>((event, emit) async {
      emit(const ForecastLoadingState(
          message: 'Searching forecast in next few days..'));
      Either<FailGetForecast, ForecastFiveDaysModel> response =
          await getCurrentForecastUsecase.call(lat: event.lat, lon: event.lon);

      response.fold(
          (exception) => emit(ForecastErrorState(message: exception.message)),
          (ForecastFiveDaysModel forecastFiveDaysModel) {
        forecastFiveDaysModel.sortListByDate();
        return emit(
            ForecastLoadedState(forecastFiveDaysModel: forecastFiveDaysModel));
      });
    });
  }
}
