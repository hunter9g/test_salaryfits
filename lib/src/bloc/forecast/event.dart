// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class ForecastEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class ForecastFetchList extends ForecastEvent {
  final double lat;
  final double lon;
  ForecastFetchList({
    required this.lat,
    required this.lon,
  });
}

class CurrentForecastFetch extends ForecastEvent {
  final double lat;
  final double lon;
  CurrentForecastFetch({
    required this.lat,
    required this.lon,
  });
}

class SetLoadingForecast extends ForecastEvent {
  final String message;

  SetLoadingForecast({required this.message});
}

class SearchForForecast extends ForecastEvent {
  final String? name;
  final String? id;

  SearchForForecast({this.name, this.id});
}
