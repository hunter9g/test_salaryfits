import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/user_preferences/event.dart';
import 'package:forecasting/src/bloc/user_preferences/state.dart';

class UserPreferencesBloc
    extends Bloc<UserPreferencesEvent, UserPreferencesState> {
  UserPreferencesModel userPreferencesModel =
      const UserPreferencesModel.empty();

  UserPreferencesBloc()
      : super(const UserPreferencesLoadedState(
            userPreferencesModel: UserPreferencesModel.empty())) {
    on<ChangePreference>((event, emit) async {
      userPreferencesModel = userPreferencesModel.copyWith(
          userLocale: event.userPreferencesModel.userLocale,
          themeMode: event.userPreferencesModel.themeMode);

      emit(UserPreferencesLoadedState(
          userPreferencesModel: userPreferencesModel));
    });
  }
}
