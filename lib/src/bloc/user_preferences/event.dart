import 'package:equatable/equatable.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';

abstract class UserPreferencesEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class ChangePreference extends UserPreferencesEvent {
  final UserPreferencesModel userPreferencesModel;
  ChangePreference({required this.userPreferencesModel});
}
