import 'package:equatable/equatable.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';

abstract class UserPreferencesState extends Equatable {
  const UserPreferencesState();
}

class UserPreferencesLoadingState extends UserPreferencesState {
  final String message;

  const UserPreferencesLoadingState({required this.message});
  @override
  List<Object?> get props => [message];
}

class UserPreferencesErrorState extends UserPreferencesState {
  final String message;

  const UserPreferencesErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class UserPreferencesLoadedState extends UserPreferencesState {
  final UserPreferencesModel userPreferencesModel;

  const UserPreferencesLoadedState({required this.userPreferencesModel});

  @override
  List<Object?> get props => [userPreferencesModel];
}
