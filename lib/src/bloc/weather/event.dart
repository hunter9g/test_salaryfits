// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class WeatherEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class WeatherFetchList extends WeatherEvent {
  final double lat;
  final double lon;
  WeatherFetchList({
    required this.lat,
    required this.lon,
  });
}

class WeatherForecastFetch extends WeatherEvent {
  final double lat;
  final double lon;
  WeatherForecastFetch({
    required this.lat,
    required this.lon,
  });
}

class SetLoadingWeather extends WeatherEvent {
  final String message;

  SetLoadingWeather({required this.message});
}
