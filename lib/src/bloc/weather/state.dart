import 'package:equatable/equatable.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();
}

class WeatherLoadingState extends WeatherState {
  final String message;

  const WeatherLoadingState({required this.message});
  @override
  List<Object?> get props => [message];
}

class WeatherErrorState extends WeatherState {
  final String message;

  const WeatherErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class WeatherLoadedState extends WeatherState {
  final CurrentForecastModel currentForecastModel;

  const WeatherLoadedState({required this.currentForecastModel});

  @override
  List<Object?> get props => [currentForecastModel];
}
