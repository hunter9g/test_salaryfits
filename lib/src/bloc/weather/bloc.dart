import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/current_weather.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_current_weather.dart';
import 'event.dart';
import 'state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final GetCurrentWeatherUsecase getCurrentWeatherUsecase;

  WeatherBloc({required this.getCurrentWeatherUsecase})
      : super(const WeatherLoadingState(message: 'Searching for weather..')) {
    on<SetLoadingWeather>((event, emit) {
      emit(WeatherLoadingState(message: event.message));
    });
    on<WeatherForecastFetch>((event, emit) async {
      emit(const WeatherLoadingState(message: 'Searching for weather..'));
      Either<FailGetForecast, CurrentForecastModel> response =
          await getCurrentWeatherUsecase.call(lat: event.lat, lon: event.lon);

      response.fold(
          (exception) => emit(WeatherErrorState(message: exception.message)),
          (CurrentForecastModel currentForecastModel) => emit(
              WeatherLoadedState(
                  currentForecastModel: currentForecastModel)));
    });
  }
}
