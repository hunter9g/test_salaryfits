import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';
import 'package:forecasting/modules/forecast/domain/errors/errors.dart';
import 'package:forecasting/modules/forecast/domain/usecases/get_location.dart';
import 'package:forecasting/src/bloc/location/event.dart';
import 'package:forecasting/src/bloc/location/state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  GetLocationUsecase getLocationUsecase;
  LocationBloc({required this.getLocationUsecase})
      : super(const LocationLoadingState(message: 'Searching for location..')) {
    on<LocationFetch>((event, emit) async {
      emit(const LocationLoadingState(message: 'Searching for location..'));
      Either<FailGetLocation, LocationModel> locationUsecase =
          await getLocationUsecase.call();

      locationUsecase.fold(
          (exception) => emit(LocationErrorState(message: exception.message)),
          (LocationModel locationModel) {
        emit(LocationLoadedState(locationModel: locationModel));
      });
    });
  }
}
