import 'package:equatable/equatable.dart';

abstract class LocationEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class LocationFetch extends LocationEvent {
  LocationFetch();
}
