import 'package:equatable/equatable.dart';
import 'package:forecasting/modules/forecast/data/model/location.dart';

abstract class LocationState extends Equatable {
  const LocationState();
}

class LocationLoadingState extends LocationState {
  final String message;

  const LocationLoadingState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LocationErrorState extends LocationState {
  final String message;

  const LocationErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class LocationLoadedState extends LocationState {
  final LocationModel locationModel;

  const LocationLoadedState({required this.locationModel});

  @override
  List<Object?> get props => [locationModel];
}
