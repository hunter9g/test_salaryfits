import 'package:flutter/material.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/components/bottom_sheet_options.dart';

class Utils {
  final BuildContext buildContext;
  const Utils({required this.buildContext});

  Future<void> showModalBottomSheetOptionsSharedPreferences(
      {required UserPreferencesModel userPreferencesModel,
      required UserPreferencesBloc userPreferencesBloc}) async {
    return showModalBottomSheet<void>(
      context: buildContext,
      builder: (BuildContext context) {
        return BottomSheetUserPreferencesOptions(
            userPreferencesBloc: userPreferencesBloc,
            userPreferencesModel: userPreferencesModel);
      },
    );
  }
}
