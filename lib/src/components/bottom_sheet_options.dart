// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:forecasting/helpers/hex_color.dart';

import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:forecasting/src/bloc/user_preferences/bloc.dart';
import 'package:forecasting/src/bloc/user_preferences/event.dart';
import 'package:localization/localization.dart';

class BottomSheetUserPreferencesOptions extends StatelessWidget {
  final UserPreferencesBloc userPreferencesBloc;
  final UserPreferencesModel userPreferencesModel;
  const BottomSheetUserPreferencesOptions({
    Key? key,
    required this.userPreferencesBloc,
    required this.userPreferencesModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: HexColor.createMaterialColor(Theme.of(context).primaryColor)[100],
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              children: [
                Flexible(
                    child: SwitchListTile(
                      activeColor: Colors.white,
                        title: Text('dark-theme'.i18n(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                        value: userPreferencesModel.isDarkTheme(),
                        onChanged: (bool bool) {
                          Navigator.of(context).pop();

                          userPreferencesBloc.add(ChangePreference(
                              userPreferencesModel:
                                  userPreferencesModel.copyWith(
                                      themeMode:
                                          userPreferencesModel.isDarkTheme()
                                              ? ThemeMode.light
                                              : ThemeMode.dark)));
                        }))
              ],
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();

                userPreferencesBloc.add(ChangePreference(
                    userPreferencesModel: userPreferencesModel.copyWith(
                        userLocale: const Locale('pt', 'BR'))));
              },
              trailing:
                  userPreferencesModel.userLocale.toLanguageTag() == 'pt-BR'
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                        )
                      : null,
              title: Text(
                'portuguese'.i18n(),
                style: const TextStyle(color: Colors.white),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();

                userPreferencesBloc.add(ChangePreference(
                    userPreferencesModel: userPreferencesModel.copyWith(
                        userLocale: const Locale('en', 'US'))));
              },
              trailing:
                  userPreferencesModel.userLocale.toLanguageTag() == 'en-US'
                      ? const Icon(
                          Icons.check,
                          color: Colors.white,
                        )
                      : null,
              title: Text(
                'english'.i18n(),
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
