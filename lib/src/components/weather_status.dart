// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:localization/localization.dart';

import 'package:forecasting/modules/forecast/data/model/current_weather.dart';

class CurrentWeatherItems {
  final String name;
  final String value;
  final Widget img;
  CurrentWeatherItems({
    required this.name,
    required this.value,
    required this.img,
  });
}

class WeatherStatus extends StatelessWidget {
  final CurrentForecastModel currentForecastModel;
  const WeatherStatus({super.key, required this.currentForecastModel});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ...[
          CurrentWeatherItems(
              name: 'humidity'.i18n(),
              value:
                  '${currentForecastModel.mainModel!.humidityModel!.toDouble()} %',
              img: Image.asset('assets/img/humidity.png')),
          CurrentWeatherItems(
              name: 'wind-speed'.i18n(),
              value:
                  '${currentForecastModel.windModel!.speedModel!.toDouble()} km/h',
              img: Image.asset('assets/img/wind.png')),
          CurrentWeatherItems(
              name: 'clouds'.i18n(),
              value:
                  '${currentForecastModel.cloudsModel!.allModel!.toDouble()}',
              img: Image.asset('assets/img/cloud.png')),
        ].map((CurrentWeatherItems e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        e.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      IconButton(
                        icon: e.img,
                        onPressed: () {},
                      ),
                      Text(
                        e.value,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      )
                    ]),
              ),
            )),
      ],
    );
  }
}
