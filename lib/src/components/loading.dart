import 'package:flutter/material.dart';

class LoadingMessage extends StatelessWidget {
  final String message;
  const LoadingMessage(this.message, {super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircularProgressIndicator(
            color: Colors.white,
          ),
          const SizedBox(
            height: 20.0,
          ),
          Text(
            message.toString(),
            style: const TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
