import 'package:flutter/material.dart';
import 'package:forecasting/helpers/hex_color.dart';
import 'package:forecasting/modules/forecast/data/model/forecast_five_days.dart';
import 'package:forecasting/modules/forecast/data/model/user_preferences.dart';
import 'package:localization/localization.dart';

class WeatherCard extends StatelessWidget {
  final DataModel dataModel;
  final UserPreferencesModel userPreferencesModel;
  const WeatherCard(
      {super.key, required this.dataModel, required this.userPreferencesModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Theme.of(context).primaryColor,
            HexColor.createMaterialColor(Theme.of(context).primaryColor)[200]!,
          ],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300]!,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                dataModel.mainModel!
                    .tempToAny(locale: userPreferencesModel.userLocale),
                style: const TextStyle(fontSize: 54, color: Colors.white),
              ),
              Text(
                dataModel.returnHourInStringFromTimeStamp(),
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                '${'wind-speed'.i18n()} ${dataModel.windModel!.speedModel.toString()} km/h',
                style: const TextStyle(fontSize: 12, color: Colors.white),
              ),
              Text(
                '${'humidity'.i18n()} ${dataModel.mainModel!.humidityModel.toString()}%',
                style: const TextStyle(fontSize: 12, color: Colors.white),
              ),
              Text(
                dataModel.weatherModel![0].descriptionModel.toString(),
                style: const TextStyle(fontSize: 16, color: Colors.white),
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              dataModel.weatherModel![0].returnImageBasedOnWeather(),
              Text(
                dataModel.weatherModel![0].mainModel!.toLowerCase().i18n(),
                style: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              )
            ],
          )
        ]),
      ),
    );
  }
}
